# Projekt - obesenec v konzoli
 
 První zkušební projekt po asi třech týdnech učení programování v Pythonu.

 Popis:
 -Klasická hra oběšenec v jednoduchém konzolém podání.
 -Možnost volby tří obtížností hádaných slov.
    -lehká (slova o třech písmenech)
    -střední (slova o pěti písmenech)
    -těžká (slova o osmi písmenech)
 -Grafické znázornění pozice uhodnutých písmen.
 
 
Jakub Kolář e:\\kolarkuba@gmail.com
